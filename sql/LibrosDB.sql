CREATE DATABASE LibrosDB;
USE LibrosDB;

CREATE TABLE Usuarios (
    id_usuario INT AUTO_INCREMENT PRIMARY KEY,
    nombre_usuario VARCHAR(100),
    email VARCHAR(100),
    contraseña VARCHAR(100),
    valoracion FLOAT,
    num_valoraciones INT
);

CREATE TABLE Libros (
    id_libro INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(200),
    autor VARCHAR(100),
    isbn VARCHAR(20)
);

CREATE TABLE EnlacesCompra (
    id_enlace INT AUTO_INCREMENT PRIMARY KEY,
    id_libro INT,
    enlace VARCHAR(255),
    FOREIGN KEY (id_libro) REFERENCES Libros(id_libro)
);

CREATE TABLE LibrosSegundaMano (
    id_libro_segunda INT AUTO_INCREMENT PRIMARY KEY,
    id_libro INT,
    id_usuario INT,
    descripcion TEXT,
    precio FLOAT,
    foto VARCHAR(255),
    FOREIGN KEY (id_libro) REFERENCES Libros(id_libro),
    FOREIGN KEY (id_usuario) REFERENCES Usuarios(id_usuario)
);

CREATE TABLE Valoraciones (
    id_valoracion INT AUTO_INCREMENT PRIMARY KEY,
    id_valorador INT,
    id_valorado INT,
    valoracion FLOAT,
    FOREIGN KEY (id_valorador) REFERENCES Usuarios(id_usuario),
    FOREIGN KEY (id_valorado) REFERENCES Usuarios(id_usuario)
);

CREATE TABLE Relatos (
    id_relato INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    titulo VARCHAR(200),
    contenido_pdf VARCHAR(255),
    FOREIGN KEY (id_usuario) REFERENCES Usuarios(id_usuario)
);

CREATE TABLE Seguidores (
    id_seguidor INT,
    id_seguido INT,
    PRIMARY KEY (id_seguidor, id_seguido),
    FOREIGN KEY (id_seguidor) REFERENCES Usuarios(id_usuario),
    FOREIGN KEY (id_seguido) REFERENCES Usuarios(id_usuario)
);
