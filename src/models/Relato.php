<?php
class Relato {
    private $id;
    private $idUsuario;
    private $titulo;
    private $contenidoPdf;

    public function __construct($id, $idUsuario, $titulo, $contenidoPdf) {
        $this->id = $id;
        $this->idUsuario = $idUsuario;
        $this->titulo = $titulo;
        $this->contenidoPdf = $contenidoPdf;
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getContenidoPdf() {
        return $this->contenidoPdf;
    }

    // Setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setContenidoPdf($contenidoPdf) {
        $this->contenidoPdf = $contenidoPdf;
    }
}
