<?php
class Seguidor {
    private $id;
    private $idSeguidor;
    private $idSeguido;

    public function __construct($id, $idSeguidor, $idSeguido) {
        $this->id = $id;
        $this->idSeguidor = $idSeguidor;
        $this->idSeguido = $idSeguido;
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getIdSeguidor() {
        return $this->idSeguidor;
    }

    public function getIdSeguido() {
        return $this->idSeguido;
    }

    // Setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setIdSeguidor($idSeguidor) {
        $this->idSeguidor = $idSeguidor;
    }

    public function setIdSeguido($idSeguido) {
        $this->idSeguido = $idSeguido;
    }
}
