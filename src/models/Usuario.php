<?php
require_once 'Database.php';

class Usuario {
    private $conn;
    private $table_name = "Usuarios";

    // Propiedades del objeto
    public $id;
    public $nombre;
    public $email;
    public $contraseña;

    // constructor
    public function __construct() {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    // método para registrar un nuevo usuario
    public function registrar($nombre, $email, $contraseña) {
        // prepara la consulta
        $query = "INSERT INTO " . $this->table_name . " (nombre_usuario, email, contraseña) VALUES (?, ?, ?)";

        $stmt = $this->conn->prepare($query);

        // protege contra la inyección de SQL
        $nombre = htmlspecialchars(strip_tags($nombre));
        $email = htmlspecialchars(strip_tags($email));
        $contraseña = htmlspecialchars(strip_tags($contraseña));

        // haz un hash de la contraseña antes de guardarla en la base de datos
        $contraseña_hashed = password_hash($contraseña, PASSWORD_BCRYPT);

        // vincula los valores
        $stmt->bind_param("sss", $nombre, $email, $contraseña_hashed);

        // ejecuta la consulta
        if($stmt->execute()) {
            return true;
        }

        return false;
    }
    
    // método para obtener un usuario
    public function obtener($email, $contraseña) {
        // prepara la consulta
        $query = "SELECT * FROM " . $this->table_name . " WHERE email = ?";

        $stmt = $this->conn->prepare($query);

        // vincula los valores
        $stmt->bind_param("s", $email);

        // ejecuta la consulta
        if($stmt->execute()) {
            $result = $stmt->get_result();
            $usuario = $result->fetch_object();
            
            $contraseña_hashed = $usuario->contraseña;
    
            if(password_verify($contraseña, $contraseña_hashed)){
                return $usuario;
            }
        }
    
        return null;
    }
}
