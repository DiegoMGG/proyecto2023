<?php
class LibroSegundaMano {
    private $id;
    private $idLibro;
    private $idUsuario;
    private $descripcion;
    private $precio;
    private $foto;

    public function __construct($id, $idLibro, $idUsuario, $descripcion, $precio, $foto) {
        $this->id = $id;
        $this->idLibro = $idLibro;
        $this->idUsuario = $idUsuario;
        $this->descripcion = $descripcion;
        $this->precio = $precio;
        $this->foto = $foto;
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getIdLibro() {
        return $this->idLibro;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getPrecio() {
        return $this->precio;
    }

    public function getFoto() {
        return $this->foto;
    }

    // Setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setIdLibro($idLibro) {
        $this->idLibro = $idLibro;
    }

    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setPrecio($precio) {
        $this->precio = $precio;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }
}
