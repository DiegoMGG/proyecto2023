<?php
class EnlaceCompra {
    private $id;
    private $idLibro;
    private $enlace;

    public function __construct($id, $idLibro, $enlace) {
        $this->id = $id;
        $this->idLibro = $idLibro;
        $this->enlace = $enlace;
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getIdLibro() {
        return $this->idLibro;
    }

    public function getEnlace() {
        return $this->enlace;
    }

    // Setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setIdLibro($idLibro) {
        $this->idLibro = $idLibro;
    }

    public function setEnlace($enlace) {
        $this->enlace = $enlace;
    }
}
