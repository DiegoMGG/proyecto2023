<?php

class Database {
    // Propiedades de la base de datos
    private $host = "localhost";
    private $db_name = "LibrosDB";
    private $username = "admin";
    private $password = "secreto";
    public $conn;

    // Obtener la conexión a la base de datos
    public function getConnection() {
        $this->conn = null;

        try {
            $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        } catch (mysqli_sql_exception $exception) {
            echo "Error de conexión: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
