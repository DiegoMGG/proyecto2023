<?php
class Valoracion {
    private $id;
    private $idValorador;
    private $idValorado;
    private $valoracion;

    public function __construct($id, $idValorador, $idValorado, $valoracion) {
        $this->id = $id;
        $this->idValorador = $idValorador;
        $this->idValorado = $idValorado;
        $this->valoracion = $valoracion;
    }

    // Getters
    public function getId() {
        return $this->id;
    }

    public function getIdValorador() {
        return $this->idValorador;
    }

    public function getIdValorado() {
        return $this->idValorado;
    }

    public function getValoracion() {
        return $this->valoracion;
    }

    // Setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setIdValorador($idValorador) {
        $this->idValorador = $idValorador;
    }

    public function setIdValorado($idValorado) {
        $this->idValorado = $idValorado;
    }

    public function setValoracion($valoracion) {
        $this->valoracion = $valoracion;
    }
}
