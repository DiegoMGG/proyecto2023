<?php
    // Directorio donde están los archivos PDF
    $pdfDir = '../../public/images/pdfs';

    // Leer el contenido del directorio
    $files = scandir($pdfDir);

    // Filtrar solo archivos .pdf
    $pdfFiles = array_filter($files, function($file) use ($pdfDir) {
        return is_file("$pdfDir/$file") && pathinfo("$pdfDir/$file", PATHINFO_EXTENSION) === 'pdf';
    });

    // Enviar la lista de archivos PDF como respuesta JSON
    header('Content-Type: application/json');
    echo json_encode(array_values($pdfFiles));
?>
