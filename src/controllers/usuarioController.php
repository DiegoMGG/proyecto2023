<?php

class usuarioController {
    private $conn;
    private $table_name = "Usuarios";

    // constructor
    public function __construct() {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    // método para registrar un nuevo usuario
    public function registrar($nombre, $email, $password) {
        // prepara la consulta
        $query = "INSERT INTO " . $this->table_name . " (nombre_usuario, email, contraseña) VALUES (?, ?, ?)";

        $stmt = $this->conn->prepare($query);

        // protege contra la inyección de SQL
        $nombre = htmlspecialchars(strip_tags($nombre));
        $email = htmlspecialchars(strip_tags($email));
        $password = htmlspecialchars(strip_tags($password));

        // haz un hash de la contraseña antes de guardarla en la base de datos
        $password_hashed = password_hash($password, PASSWORD_BCRYPT);

        // vincula los valores
        $stmt->bind_param("sss", $nombre, $email, $password_hashed);

        // ejecuta la consulta
        if($stmt->execute()) {
            return true;
        }

        return false;
    }
}