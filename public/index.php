<?php
    // obtén los datos del cuerpo de la petición
    $data = json_decode(file_get_contents("php://input"));

    // asume que 'ruta' es una propiedad del objeto JSON que recibiste
    $ruta = $data->ruta;

    switch ($ruta) {
        case 'registro':
            // requiere la clase Usuario
            require_once '../src/models/Usuario.php';
            //require_once '../src/controller/usuarioController.php';

            // crea una nueva instancia de la clase Usuario
            $usuario = new Usuario();

            // llama al método registrar
            $usuario->registrar($data->nombre, $data->email, $data->password);
            break;
        case 'obtener':
            // requiere la clase Usuario
            require_once '../src/models/Usuario.php';

            // crea una nueva instancia de la clase Usuario
            $usuario = new Usuario();

            // llama al método obtener
            $result = $usuario->obtener($data->email, $data->password);

            header('content-type:application/json');

            echo json_encode($result);
            break;
    }
?>