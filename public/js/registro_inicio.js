window.onload = function() {
    document.getElementById('formRegistro').addEventListener('submit', function(event) {
        event.preventDefault();

        var nombre = document.getElementById('nombreRegistro').value;
        var email = document.getElementById('emailRegistro').value;
        var password = document.getElementById('passwordRegistro').value;

        fetch('http://127.0.0.1/Proyecto2023/public/index.php', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                ruta: 'registro',
                nombre: nombre,
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {console.log('Success:', data);})
        .catch((error) => {console.error('Error:', error);});
    });

    document.getElementById('formInicioSesion').addEventListener('submit', function(event) {
        event.preventDefault();

        var email = document.getElementById('emailInicioSesion').value;
        var password = document.getElementById('passwordInicioSesion').value;

        fetch('http://127.0.0.1/Proyecto2023/public/index.php', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                ruta: 'obtener',
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {console.log('Success:', data);})
        .catch((error) => {console.error('Error:', error);});
    });
};
